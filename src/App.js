import React, {Suspense, useState} from "react";
import CssBaseline from '@material-ui/core/CssBaseline'
import Table from '../src/components/Table'
import './App.css'
import {parse} from 'papaparse'

const App = () => {
    const [isDragged, setIsDragged] = useState(false)
    const [data, setData] = useState([])
    const [dataHead, setDataHead] = useState([])
    const [fileTypeIsValid, setFileTypeIsValid] = useState(false)
    const [isDropped, setIsDropped] = useState(false)

    const onDrop = async (e) => {
        e.preventDefault();
        setIsDropped(true)

        if (e.dataTransfer.files[0].type === 'text/csv') {
            const text = await e.dataTransfer.files[0].text();

            if (checkFileIsCorrect(text.split('\n', 1)[0])) {
                setFileTypeIsValid(true);
                const result = parse(text, {header: true})
                addDuplicate(result.data)
                setDataHead(Object.keys(result.data[0]))
                setData(result.data)
            }
        }
    }

    const addDuplicate = (arrOfObj) => {
        arrOfObj.forEach((element, i) => {
            const index1 = findIndex(element, i, arrOfObj, 'email')
            const index2 = findIndex(element, i, arrOfObj, 'phone')

            if (index1 === -1 && index2 === -1) {
                element['Duplicate'] = '---'
            } else {
                element['Duplicate'] = (1 + Math.min(index1, index2)).toString()
            }
        })
    }

    const findIndex = (element, index, array, fieldNameWhoSearch) => {
        let fieldName = null

        Object.keys(element).forEach((value) => {
            if (value.toLowerCase() === fieldNameWhoSearch) {
                fieldName = value
            }
        })

        for (let i = 0; i < array.length; i++) {
            if (i !== index && array[i][fieldName].toLowerCase() === element[fieldName].toLowerCase()) {
                return i
            }
        }
        return -1
    }


    const checkFileIsCorrect = (text) => {
        const textLower = text.toLowerCase()
        if (textLower.search('full name') === -1) {
            return false
        } else if (textLower.search('email') === -1) {
            return false
        } else if (textLower.search('phone') === -1) {
            return false
        }
        return true
    }

    return (
        <Suspense fallback={<p>Loading...</p>}>
            <CssBaseline/>
            <div
                id={isDragged ? 'id_file_receiver' : null}
                className='file_receiver'
                onDragEnter={() => {
                    setIsDragged(true)
                }}
                onDragLeave={() => {
                    setIsDragged(false)
                }}
                onDragOver={e => e.preventDefault()}
                onDrop={onDrop}
            >
                DROP HERE
            </div>
            {
                fileTypeIsValid && isDropped ?
                    <Table data={data} headData={dataHead}/>
                    : isDropped ? <div>File type is not valid!</div>
                    : null
            }
        </Suspense>
    );
}

export default App;
