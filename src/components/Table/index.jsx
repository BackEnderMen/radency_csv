import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import MUTableHead from './MUTableHead'


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        borderRadius: theme.spacing(2),
    },
    paper: {
        width: '100%',
        borderRadius: theme.spacing(2),
    },
    table: {
        minWidth: 700,
    },
    tableBody: {
        background: 'red',
    },
    row: {
        background: '#34558b',
        '&:hover': {
            background: '#325082',
        },
    },
    cell: {
        color: 'white',
        margin: 0,
    },
    errorCell: {
        color: 'white',
        margin: 0,
        background: 'red',
        border: 'solid 3px black'
    }
}));


const MaTable = ({data, headData}) => {
    const classes = useStyles();


    const isValid = (key, value, index) => {

        if (key.toLowerCase() === 'age') {
            if (!((/^-?\d+$/.test(value) && value >= 21) || value === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'experience') {
            if (!((!isNaN(value) && value >= 0 && value < data[index]['Age']) || value === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'yearly income') {
            if (!((!isNaN(value) && value < 1e9 && value.toString().includes('.') && value.toString().split('.')[1].length === 2) || value === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'phone') {
            if (!(value.replace('+1', '').length === 10 || value === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'has children') {
            const valueLower = value.toLowerCase()
            if (!(valueLower === 'false' || valueLower === 'true' || valueLower === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'license number') {
            if (!(value.length === 6 || value === '')) {
                return false
            }
        } else if (key.toLowerCase() === 'expiration date') {
            const dateA = value.split('-');
            const dateB = value.split('/');
            const dateNow = Date.now();
            if (value === '') return true
            if (value.includes('-')) {
                if (!(dateA.length === 3 && dateA[0].length === 4 && dateA[1].length === 2 && dateA[2].length === 2)) {
                    return false
                }
            } else if (!(dateB.length === 3 && dateB[0].length === 2 && dateB[1].length === 2 && dateB[2].length === 4)) {
                return false
            }
            const date = new Date(value)
            if (date > dateNow) {
                return false
            }
        }

        return true
    }

    const splitLicenseStates = (str) => {
        return str.split('|').map((value) => {
            const short = value[0] + value[1] + ' '
            return short.toUpperCase()
        })
    }

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table className={classes.table}>
                        <MUTableHead headCells={headData}/>
                        <TableBody>
                            {data?.map((row, index) => {
                                const labelId = `enhanced-table-${index}`;
                                const length = Object.entries(row).length
                                return (
                                    <TableRow key={labelId} className={classes.row}>
                                        <TableCell component="th" className={classes.cell} scope="row" align={'left'}
                                                   key={'idTableCell'}>
                                            {index + 1}
                                        </TableCell>
                                        {
                                            Object.entries(row).map(([key, value], i) =>
                                                <TableCell component="th"
                                                           className={isValid(key, value, index) ? classes.cell : classes.errorCell}
                                                           scope="row"
                                                           align={i === 0 ? 'center' : i + 1 === length ? 'right' : 'center'}
                                                           key={labelId + i}
                                                >
                                                    {key.toLowerCase() === 'has children' && value === '' ?
                                                        'FALSE' : key.toLowerCase() === 'license states' ?
                                                            splitLicenseStates(value.trim()) : value.trim()}
                                                </TableCell>
                                            )
                                        }
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    );
};

export default MaTable