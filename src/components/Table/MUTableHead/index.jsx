import React from 'react';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import PropTypes from 'prop-types';

const MUTableHead = ({headCells}) => {
    const length = headCells.length;

    return (
        <TableHead>
            <TableRow>
                <TableCell size={'small'} key={'HeaderCellId'} align={'left'}>
                    Index
                </TableCell>
                {headCells.map((headCell, i) => (
                    <TableCell
                        size={'small'}
                        key={'HeaderCell' + headCell + i}
                        align={i === 0 ? 'center' : i + 1 === length ? 'right' : 'center'}
                    >
                        {headCell.trim()}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
};

export default MUTableHead

MUTableHead.propTypes = {
    headCells: PropTypes.array.isRequired,
};
